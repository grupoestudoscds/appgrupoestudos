package br.com.cdssoftware.appgrupoestudos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DespesaFragment extends Fragment {

    public DespesaFragment() {
        // Required empty public constructor
    }

    public static DespesaFragment newInstance() {
        DespesaFragment fragment = new DespesaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_despesa, container, false);
    }

}
